package piedraPapelTijera;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		objetoDeJuego = PiedraPapelTijeraEnum.TIJERA;
		
	}

	@Override
	public GanaPierdeEmpataEnum comparar(PiedraPapelTijeraFactory pPiePaTij) {
	GanaPierdeEmpataEnum resultado;
		
		switch (pPiePaTij.getObjetoDeJuego()) {
		//gana
		case PAPEL:
			descripcionResultado = objetoDeJuego.getNombre() + " le gana a " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.GANA;					
			break;
		//pierde	
		case PIEDRA:
			descripcionResultado = objetoDeJuego.getNombre() + " pierde con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.PIERDE;
			break;
		
		default:
			descripcionResultado = objetoDeJuego.getNombre() + " empata con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.EMPATA;
			break;
			
		}
		
		return resultado;
	}

	@Override
	public boolean isMe(PiedraPapelTijeraEnum pObjetoDeJuego) {
		return pObjetoDeJuego == PiedraPapelTijeraEnum.TIJERA;
	}

}
