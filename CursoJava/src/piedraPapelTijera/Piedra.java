package piedraPapelTijera;

public class Piedra extends PiedraPapelTijeraFactory {
	
	public Piedra() {
		super();
		objetoDeJuego = PiedraPapelTijeraEnum.PIEDRA;
	}

	@Override
	public GanaPierdeEmpataEnum comparar(PiedraPapelTijeraFactory pPiePaTij) {
		GanaPierdeEmpataEnum resultado;
		
		switch (pPiePaTij.getObjetoDeJuego()) {
		//gana	
		case TIJERA:
			descripcionResultado = objetoDeJuego.getNombre() + " le gana a " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.GANA;
			break;
		//pierde
		case PAPEL:
			descripcionResultado = objetoDeJuego.getNombre() + " pierde con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.PIERDE;					
			break;		
		default:
			descripcionResultado = objetoDeJuego.getNombre() + " empata con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.EMPATA;
			break;
			
		}
		
		return resultado;
	}

	@Override
	public boolean isMe(PiedraPapelTijeraEnum pObjetoDeJuego) {		
		return pObjetoDeJuego == PiedraPapelTijeraEnum.PIEDRA;
	}

}
