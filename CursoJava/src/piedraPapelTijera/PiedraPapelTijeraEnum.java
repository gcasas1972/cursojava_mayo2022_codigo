package piedraPapelTijera;

public enum PiedraPapelTijeraEnum {
	PIEDRA("PIEDRA"),
	PAPEL("PAPEL"),
	TIJERA("TIJERA");
	
	private final String nombre;
	
	private PiedraPapelTijeraEnum(String pNombre){
		nombre = pNombre;
	}
	
	public String getNombre(){
		return nombre;
	}
}
