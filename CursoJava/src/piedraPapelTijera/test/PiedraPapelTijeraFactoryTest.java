package piedraPapelTijera.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import piedraPapelTijera.GanaPierdeEmpataEnum;
import piedraPapelTijera.Papel;
import piedraPapelTijera.Piedra;
import piedraPapelTijera.PiedraPapelTijeraEnum;
import piedraPapelTijera.PiedraPapelTijeraFactory;
import piedraPapelTijera.Tijera;

public class PiedraPapelTijeraFactoryTest {
	//lote de pruebas
	PiedraPapelTijeraFactory piedra, papel, tijera;
	

	@Before
	public void setUp() throws Exception {
		piedra = new Piedra();
		papel  = new Papel();
		tijera = new Tijera();
	}

	@After
	public void tearDown() throws Exception {
		piedra 	= null;
		papel 	= null;
		tijera 	= null;
				
	}

	@Test
	public void testGetInstance_piedra() {
		assertTrue( PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraEnum.PIEDRA) instanceof Piedra );
	}
	@Test
	public void testGetInstance_papel() {
		assertTrue( PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraEnum.PAPEL) instanceof Papel );
	}

	@Test
	public void testGetInstance_tijera() {
		assertTrue( PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraEnum.TIJERA) instanceof Tijera);
	}

	@Test
	public void testCompararPiedrPierdConPapel() {
		assertEquals(GanaPierdeEmpataEnum.PIERDE, piedra.comparar(papel));
		assertEquals("PIEDRA pierde con PAPEL", piedra.getDescripcionResultado());
		
		System.out.println(piedra.getDescripcionResultado());
		
	}
	@Test
	public void testCompararPiedraLeGanaaTijera() {
		assertEquals(GanaPierdeEmpataEnum.GANA, piedra.comparar(tijera));
		assertEquals("PIEDRA le gana a TIJERA", piedra.getDescripcionResultado());
		
		System.out.println(piedra.getDescripcionResultado());
		
	}
	
	

}
