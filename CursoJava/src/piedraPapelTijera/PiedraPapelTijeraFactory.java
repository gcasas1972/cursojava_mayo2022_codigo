package piedraPapelTijera;


import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {
	protected String descripcionResultado;
	private static List<PiedraPapelTijeraFactory> elementos;
	protected PiedraPapelTijeraEnum objetoDeJuego;
	
	//constructor
	public PiedraPapelTijeraFactory(){}

	public PiedraPapelTijeraEnum getObjetoDeJuego() {
		return objetoDeJuego;
	}

	public void setObjetoDeJuego(PiedraPapelTijeraEnum pObjetoDeJuegog) {
		this.objetoDeJuego = pObjetoDeJuegog;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}
	
	//metodo de negocio
	public static PiedraPapelTijeraFactory getInstance(PiedraPapelTijeraEnum pObjetoDeJuego){
		
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pObjetoDeJuego))
				return piedraPapelTijeraFactory;
		}
		return null;
	}
	
	public abstract GanaPierdeEmpataEnum comparar(PiedraPapelTijeraFactory pPiePaTij);
	public abstract boolean isMe(PiedraPapelTijeraEnum pObjetoDeJuego);
	

}
