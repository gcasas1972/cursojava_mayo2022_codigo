package piedraPapelTijera;

public enum GanaPierdeEmpataEnum {
	GANA("GANA"),
	PIERDE("PIERDE"),
	EMPATA("EMPATA");
	
	private final String nombre;
	private GanaPierdeEmpataEnum(String pNombre) {
		nombre = pNombre;
	}
	public String getNombre() {
		return nombre;
	}
	
	

	

}
