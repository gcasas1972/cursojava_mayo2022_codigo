package piedraPapelTijera;

public class Papel extends PiedraPapelTijeraFactory {

	
	public Papel() {
		super();
		objetoDeJuego = PiedraPapelTijeraEnum.PAPEL;
	}

	@Override
	public GanaPierdeEmpataEnum comparar(PiedraPapelTijeraFactory pPiePaTij) {
	GanaPierdeEmpataEnum resultado;
		
		switch (pPiePaTij.getObjetoDeJuego()) {
		//gana
		case PIEDRA:
			descripcionResultado = objetoDeJuego.getNombre() + " le gana a " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.GANA;					
			break;
		//pierde	
		case TIJERA:
			descripcionResultado = objetoDeJuego.getNombre() + " pierde con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.PIERDE;
			break;
		
		default:
			descripcionResultado = objetoDeJuego.getNombre() + " empata con " + pPiePaTij.getObjetoDeJuego().getNombre();
			resultado = GanaPierdeEmpataEnum.EMPATA;
			break;
			
		}
		
		return resultado;
	}


	@Override
	public boolean isMe(PiedraPapelTijeraEnum pObjetoDeJuego) {		
		return pObjetoDeJuego == PiedraPapelTijeraEnum.PAPEL;
	}

}
