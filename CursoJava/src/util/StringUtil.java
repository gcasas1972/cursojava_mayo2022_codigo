package util;
/**
 * Esta clase permite ofrecer una servicios para trabajar con string
 * @author Gabriel
 *
 */
public class StringUtil {
	/**
	 * Ese metodo devuelve la candtidad de vocales uqe tiene el parametro
	 * @param pCadena es la cadena que se envia para analizar
	 * @return la cantida de volcaes
	 */
	public static int getCantidadDeVocales(String pCadena){
		int vocales=0;
		for (int i =0;i <pCadena.length();i++)
			if(		pCadena.toUpperCase().charAt(i)=='A' ||
					pCadena.toUpperCase().charAt(i)=='E' ||
					pCadena.toUpperCase().charAt(i)=='I' ||
					pCadena.toUpperCase().charAt(i)=='O' ||
					pCadena.toUpperCase().charAt(i)=='U' )
				vocales++;
		
		return vocales;
		
	}

}
