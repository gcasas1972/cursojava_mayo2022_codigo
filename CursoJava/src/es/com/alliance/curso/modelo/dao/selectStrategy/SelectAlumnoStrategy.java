package es.com.alliance.curso.modelo.dao.selectStrategy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.com.alliance.curso.modelo.Alumno;

public abstract class SelectAlumnoStrategy {
	protected static Alumno 		alumno			;
	protected static EqualsLike 	tipoDeConsulta	;
	protected static StringBuilder 	sql				;
	
	private static List<SelectAlumnoStrategy> listaDeEstrategias;
	
	public static String getSql(Alumno pAlu, EqualsLike pTipoDeConexion){
		alumno 			= pAlu				;
		tipoDeConsulta	= pTipoDeConexion	;
		
		sql = new StringBuilder("select  ALU_ID, ALU_APELLIDO, ALU_NOMBRE, ALU_EMAIL, "	);		
		sql			.append	       ("ALU_CONOCIMIENTOS, ALU_GIT "			)
					.append	       (" FROM ALUMNOS  "			);
		
		listaDeEstrategias = new ArrayList<SelectAlumnoStrategy>();
		listaDeEstrategias.add(new SelectVacioStrategy());		
		listaDeEstrategias.add(new SelectIdStrategy());
		
		//apellido es el primero
		listaDeEstrategias.add(new SelectApellidoEqualStrategy());
		listaDeEstrategias.add(new SelectApellidoLikeStrategy());
		//nombre
		
		listaDeEstrategias.add(new SelectNombreEqualaSinWhereStrategy());
		listaDeEstrategias.add(new SelectNombreEqualsConWhereStrategy());
		
		listaDeEstrategias.add(new SelectNombreLikeConWhereStrategy());
		listaDeEstrategias.add(new SelectNombreLikeSinWhereStrategy());
		
		boolean isUltimo =false;
		Iterator<SelectAlumnoStrategy> iter = listaDeEstrategias.iterator();
			
		while(iter.hasNext() && !isUltimo){
			SelectAlumnoStrategy estrategia = iter.next(); 
			if(estrategia.validar()){
				estrategia.addWhere();
				isUltimo = estrategia.isUltimo();
			}
		}
		
		return sql.toString();
	}
	public abstract boolean validar();
	public abstract void addWhere();
	public abstract boolean isUltimo();

}
