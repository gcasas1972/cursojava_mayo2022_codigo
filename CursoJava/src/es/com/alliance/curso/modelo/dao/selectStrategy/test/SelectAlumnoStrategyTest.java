package es.com.alliance.curso.modelo.dao.selectStrategy.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.com.alliance.curso.modelo.Alumno;
import es.com.alliance.curso.modelo.dao.selectStrategy.EqualsLike;
import es.com.alliance.curso.modelo.dao.selectStrategy.SelectAlumnoStrategy;

public class SelectAlumnoStrategyTest {
	Alumno aluVacio;
	Alumno aluId;
	Alumno aluApellido;
	Alumno aluNombre;
	Alumno aluNombreYapellido;
    
	StringBuilder sql ;
	 
	@Before
	public void setUp() throws Exception {
		aluVacio			= new Alumno();
		aluId 				= new Alumno(10);
		aluApellido 		= new Alumno(0, null,"Casas" , null, null, null);
		aluNombre 			= new Alumno(0, "Gabriel", null, null, null, null);
		aluNombreYapellido 	=  new Alumno(0, "Gabriel", "Casas", null, null, null);

		sql = new StringBuilder("select  ALU_ID, ALU_APELLIDO, ALU_NOMBRE, ALU_EMAIL, "	);		
		sql			.append	       ("ALU_CONOCIMIENTOS, ALU_GIT "			)
					.append	       (" FROM ALUMNOS  "			);

		
	}

	@After
	public void tearDown() throws Exception {

		aluVacio 			= null;
		aluId 				= null;
		aluApellido			= null;
		aluNombre			= null;
		aluNombreYapellido	= null;
		
		sql 				= null;
		
	}

	@Test
	public void testVacio() {
		assertEquals(sql.toString(), SelectAlumnoStrategy.getSql(aluVacio, EqualsLike.EQUALS));	
		
	}
	
	@Test
	public void testNULL() {
		assertEquals(sql.toString(), SelectAlumnoStrategy.getSql(null, EqualsLike.EQUALS));	
		
	}
	@Test
	public void testAlumnoId(){
		sql.append(" WHERE ALU_ID =10");
		assertEquals(sql.toString(), SelectAlumnoStrategy.getSql(aluId, EqualsLike.EQUALS));
	}
	@Test
	public void testAlumnoApellidoEquls(){
		sql.append(" WHERE ALU_APELLIDO='Casas'");
		assertEquals(sql.toString(), SelectAlumnoStrategy.getSql(aluApellido, EqualsLike.EQUALS));
	}

}
