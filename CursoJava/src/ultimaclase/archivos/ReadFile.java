package ultimaclase.archivos;
import java.io.*;
public class ReadFile {
  public static void main (String [] args) {
    // Create file
	String strParametro =args[0];  
    File file = new File(strParametro);
    File fileAleer=null;
    //1- obtengo todas las listas de archivos de un direcorio
    // file.listFiles()
    //2- leo el archivo
    //3- lo comparo contra un arraylist
    
    File[] archivos = file.listFiles();
    
    for (int i = 0; i < archivos.length; i++) {
    	if(archivos[i].getName().toUpperCase().contains("ROMEO"))
    		fileAleer = archivos[i];
    	System.out.println("lista de archivos:"+ archivos[i].getName()  );
	}
    
     try {
      // Create a buffered reader to read each line from a file.
      BufferedReader in = new BufferedReader(new FileReader(fileAleer));
      String s;

      // Read each line from the file and echo it to the screen.
      s = in.readLine();
      while (s != null) {
    	  System.out.println(s);
    	  s = in.readLine();
      }
      // Close the buffered reader, which also closes the file reader.
      in.close();

    } catch (FileNotFoundException e1) {
    // If this file does not exist
      System.err.println("File not found: " + file);

    } catch (IOException e2) {
    // Catch any other IO exceptions.
      e2.printStackTrace();
    }
  }
}
