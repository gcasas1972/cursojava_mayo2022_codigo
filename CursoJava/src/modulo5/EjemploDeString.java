package modulo5;

import java.util.Scanner;

import util.StringUtil;

public class EjemploDeString {

	public static void main(String[] args) {
		//string
		Scanner sc = new Scanner(System.in);
		System.out.println("ingrese una cadena de caracteres a codificar");
		//clave murcielago
		//      0123456789
		//el perro verde
		//56 p5200 v52d5
		String strOriginal = sc.nextLine();
		
		String strMayuscula = strOriginal.toUpperCase();
		
		String strMurcielago = strOriginal.toUpperCase().replace('M', '0')
														.replace('U', '1')
														.replace('R', '2')
														.replace('C', '3')
														.replace('I', '4')
														.replace('E', '5')
														.replace('L', '6')
														.replace('A', '7')
														.replace('G', '8')
														.replace('O', '9');
		
		System.out.println("el texto original =" 	+ strOriginal);
		System.out.println("el texto Mayusculas =" 	+ strMayuscula);
		System.out.println("el texto minuscula =" 	+ strOriginal.toLowerCase());
		System.out.println("el texto en clave =" 	+ strMurcielago);

		int vocales = StringUtil.getCantidadDeVocales(strOriginal);
		System.out.println("la cantidad de vocales es " + vocales);
		

	}

}
