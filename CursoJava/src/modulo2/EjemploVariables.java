package modulo2;

public class EjemploVariables {

	public static void main(String[] args) {
		byte b=127;
		//hacia arriba
		//auto cast byte->short->int->long-> float -> double 
		//            8   16      32   64     32       64
		// IEEE 1.000.000.000                1x10^9
		int i =b;
		double d =b;
		long l2=75468;
		
		//downcast
		int i2=(int)l2;
	//hacia abajo el obligatorio castear
		
		int suma = (int)l2 + b;
		
		int valor=1357;
		int valorNeg =-1357;
		
		byte bValor =(byte)valor;
		byte bValorNeg=(byte)valorNeg;
		
		//10110011 tengo que sacar el complemento a2 =-77
		//01001101
		// 64  84 1
		
		System.out.println("el valor de b =" + b);
		System.out.println("el valor de i =" +i);
		System.out.println("el valor de d =" + d);
		System.out.println("el valor de i2 =" + i2);
		
		System.out.println("suma  =" + suma);		
		System.out.println("\nvalor =" +valor);
		System.out.println("bvalor =" +bValor);
		
		System.out.println("\nvalorNeg =" +valorNeg);
		System.out.println("bvalorNeg =" +bValorNeg);
		
	}

}
