package modulo6;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import util.CalendarUtil;

public class EjemploDeCalendar {

	public static void main(String[] args) {
		Date fecha = new Date();
		//Calendar cal2 = new Calendar();
		Calendar cal = Calendar.getInstance();
		
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEEEEE-dd-MM-yyyy");
		
	
		
		System.out.println("la fecha es " + fecha);
		System.out.println("la fecha es " + sdf.format(fecha));
		
		System.out.println("\nla fecha es con calendar " + cal);
		
		System.out.println("la fecha de hoy  con calendar " + sdf.format(cal.getTime()));
		System.out.println("hoy es fin de semana?" + CalendarUtil.isFinDeSemana(cal.getTime()));
		
		
		System.out.println("el dia de una fecha es " + cal.get(Calendar.DATE));
		System.out.println("el mes de una fecha es " + (cal.get(Calendar.MONTH) +1));
		System.out.println("el mes de una fecha es " + cal.get(Calendar.YEAR) );
		
		//si al dia de hoy le sumo 3 dias
		cal.add(Calendar.DATE, 1);
		System.out.println("la fecha mas 1 dias es " + sdf.format(cal.getTime()));
		System.out.println("ma�ana es fin de semana?" + CalendarUtil.isFinDeSemana(cal.getTime()));
	}

}
